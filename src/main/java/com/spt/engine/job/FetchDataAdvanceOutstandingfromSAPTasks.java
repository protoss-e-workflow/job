package com.spt.engine.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.Date;


@Component
public class FetchDataAdvanceOutstandingfromSAPTasks {
	static final Logger LOGGER = LoggerFactory.getLogger(FetchDataAdvanceOutstandingfromSAPTasks.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    
    @Autowired
    RestTemplate restTemplate;
    
    @Value("${Url.fetchDatafromSAP_AdvanceOutstanding}")
	String urlRest ;


//    @Scheduled(cron = "* * 4 * * *")    // Every 04.00AM
//@Scheduled(cron = "5 * * * * *")    // Every 1 Min
    public void reportCurrentTime() {
    	LOGGER.info("***************************************");
    	LOGGER.info("The time is now {}" , dateFormat.format(new Date()));
    	LOGGER.info("Start fetchData and Save  from SAP AdvanceOutstanding job");
    	
    	restTemplate = new RestTemplate();


		LOGGER.info("======= URL TO SEND ==== {}",urlRest);

    	String resultString  = restTemplate.getForObject(urlRest, String.class);

    	LOGGER.info("***************************************");
    }
}
